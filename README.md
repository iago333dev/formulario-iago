# Formulario-Iago
===============================================
Descrição:

Sistema integrado ao banco de dados, tela de login com criptografia MD5 para usuario e senha,
e função cadastro de usuario com diversos campos.. ainda em andamento

-IagoAlves333_


===============================================

* para clonar o projeto na sua máquina

via https:

`git clone https://gitlab.com/90Beatzz/formulario-iago`

via ssh:

`git clone git@gitlab.com:90Beatzz/formulario-iago.git`

* Para criar uma chave ssh em sua máquina

Caso não consiga fazer o clone via https,

```
ssh-keygen -t rsa
cat ~/id_rsa.pub
```

copie o resultado e cole em sua conta do gitlab, clique no avatar do seu perfil, no canto direito superior, configurações, chaves ssh

* Para subir suas modificações local para o gitlab:

Acesse a pasta do projeto (usando o comando cd)
```
git add .
git commit -m "comentario"
git push
```

vai pedir seu login e senha do gitlab

* Para fazer o download das modificações que outra pessoa fez no gitlab para seu projeto local:

`git pull`

* clone de uma branch específica:
`git clone --single-branch --branch nome-da-branch link-do-projeto`
